import '../browser.js';
import p2 from 'p2';
const {World, Body, Box, Plane} = p2;

const world = new World({gravity: [0, -9.81]});

const box = new Body({
  mass: 1,
  position: [0, parseInt(process.argv[2])],
});
box.addShape(new Box({width: 1, height: 1}));
world.addBody(box);

const ground = new Body({
  mass: 0,
  position: [0, 0],
});
ground.addShape(new Plane());
world.addBody(ground);

const loop = (time) => {
  world.step(1 / 60);
  window.requestAnimationFrame(loop);

  printTable(time, parseInt(process.argv[2]) - box.position[1], -box.velocity[1]);
};
window.requestAnimationFrame(loop);

printTable('time', 'position', 'velocity');
console.log(''.padStart(30 * 3 + 6 * 3, '-'));
