import {exec} from 'child_process';

const libs = ['matter.js', 'cannon.js', 'p2.js', 'planck.js'];
const data = {};

let lastTime = 0;
let lastVelocity = 0;
const readRow = (end, row) => {
  const values = row.replace(/\s+([0-9\.]+)[\s|]+[0-9\.]+[\s|]+([0-9\.]+)\s*$/, '$1 $2').split(' ');
  const time = parseFloat(values[0]);
  const velocity = parseFloat(values[1]);
  if (!time && !velocity) return;

  if (velocity < lastVelocity) {
    end({
      maxVelocity: lastVelocity,
      timeOfFall: lastTime,
    });
  }
  lastVelocity = velocity;
  lastTime = time;
};

const runFreefall = (lib) =>
  new Promise((resolve) => {
    lastVelocity = 0;
    const sim = exec(`node .\\${lib}\\freefall.mjs ${process.argv[2]}`);
    let data = null;

    const cb = readRow.bind(null, (_data) => {
      data = _data;
      sim.stdout.off('data', cb);
      process.kill(sim.pid);
    });

    sim.stdout.on('data', cb);

    sim.on('exit', () => resolve(data));
  });

(async () => {
  for (const lib of libs) {
    console.log(`Simulating with ${lib}`);
    data[lib] = await runFreefall(lib);
  }
  console.table(data);
  process.exit(0);
})();
