import '../browser.js';
import Matter from 'matter-js';
const {Engine, Bodies, World, Runner} = Matter;

const engine = Engine.create({
  world: World.create(),
});

const box = Bodies.rectangle(0, 0, 10, 10);
const ground = Bodies.rectangle(-100, parseInt(process.argv[2]) * 10 + 50 / 2 + 10 / 2, 200, 50, {
  isStatic: true,
});

World.add(engine.world, [box, ground]);

const runner = Runner.create();
const loop = (time) => {
  Runner.tick(runner, engine, 1000 / 60);
  window.requestAnimationFrame(loop);

  printTable(time, box.position.y, box.velocity.y);
};
window.requestAnimationFrame(loop);

printTable('time', 'position', 'velocity');
console.log(''.padStart(30 * 3 + 6 * 3, '-'));
