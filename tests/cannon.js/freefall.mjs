import '../browser.js';

import CANNON from 'cannon';
const {World, Body, Vec3, Box, Plane} = CANNON;

const world = new World({
  gravity: new Vec3(0, 0, -9.82),
});

const box = new Body({
  mass: 1,
  position: new Vec3(0, 0, 0),
  shape: new Box(new Vec3(1, 1, 1)),
});
world.addBody(box);

const ground = new Body({
  mass: 0,
  position: new Vec3(0, 0, -parseInt(process.argv[2])),
  shape: new Plane(),
});
world.addBody(ground);

const fixedTimeStep = 1.0 / 60.0;
const maxSubSteps = 3;

let lastTime;
const loop = (time) => {
  window.requestAnimationFrame(loop);
  if (lastTime !== undefined) {
    const dt = (time - lastTime) / 1000;
    world.step(fixedTimeStep, dt, maxSubSteps);
  }
  lastTime = time;

  printTable(time, -box.position.z, -box.velocity.z);
};
window.requestAnimationFrame(loop);

printTable('time', 'position', 'velocity');
console.log(''.padStart(30 * 3 + 6 * 3, '-'));
