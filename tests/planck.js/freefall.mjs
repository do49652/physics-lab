import '../browser.js';
import planck from 'planck-js';
const {World, Vec2, Box, Edge} = planck;

const world = World({
  gravity: new Vec2(0, 9.81),
});

const box = world.createBody({
  type: 'dynamic',
  position: new Vec2(0, -1),
});
box.createFixture({
  shape: new Box(1, 1),
});

const ground = world.createBody({
  type: 'static',
  position: new Vec2(0, parseInt(process.argv[2])),
});
ground.createFixture({
  shape: new Edge(new Vec2(-40, 0), new Vec2(40, 0)),
});

let timestamp;
let lastTimestamp;
const loop = (time) => {
  window.requestAnimationFrame(loop);

  const deltaTime = time - (lastTimestamp || 0);
  lastTimestamp = time;
  timestamp = (timestamp || 0) + deltaTime;

  world.step(deltaTime / 1000);

  printTable(time, box.getPosition().y, box.getLinearVelocity().y);
};
window.requestAnimationFrame(loop);

printTable('time', 'position', 'velocity');
console.log(''.padStart(30 * 3 + 6 * 3, '-'));
