const {JSDOM} = require('jsdom');
const winObj = new JSDOM(``, {pretendToBeVisual: true}).window;

globalThis['window'] = winObj;
globalThis['document'] = winObj.document;

const padstr = (str) => `${str}`.padStart(30, ' ');
const printTable = (time, pos, vel) => {
  console.log(padstr(time) + '    |' + padstr(pos) + '    |' + padstr(vel));
};

globalThis['printTable'] = printTable;
