import React from 'react';
import ReactDOM from 'react-dom';
import 'pixi.js';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import '@fortawesome/fontawesome-free/css/all.css';

import {App} from './App';

window.PIXI = require('pixi.js');
window.teacher = localStorage.getItem('teacher') === 'true';

ReactDOM.render(React.createElement(App), document.getElementById('app'));
