import React, {FC} from 'react';
import {Layout} from './components/Layout';

export const App: FC = () => {
  return (
    <>
      <Layout />
    </>
  );
};
