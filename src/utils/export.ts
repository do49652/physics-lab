import {ExportToCsv, Options} from 'export-to-csv';
import {IData} from '../components/Table';

const options: Options = {
  fieldSeparator: ';',
  quoteStrings: '"',
  decimalSeparator: ',',
  showLabels: true,
  useTextFile: false,
  useBom: true,
  headers: [
    'Time (ms)',
    'Velocity (m/s)',
    'Velocity { x, y }',
    'Position { x, y }',
    'Angle',
    'Angular velocity',
  ],
};

export const exportData = (title: string, data: IData[]) => {
  const csvExporter = new ExportToCsv(title ? {...options, title, filename: title} : options);
  csvExporter.generateCsv(
    data.map((row) => ({
      'Time (ms)': row.time,
      'Velocity (m/s)': row.velocity,
      'Velocity { x, y }': row.velocityVec2,
      'Position { x, y }': row.positionVec2,
      Angle: row.angle,
      'Angular velocity': row.angularVelocity,
    })),
  );
};
