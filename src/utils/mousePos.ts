class MousePos {
  x = 0;
  y = 0;

  constructor() {
    document.onmousemove = (event): void => {
      let eventDoc, doc, body;

      // @ts-ignore
      event = event || window.event;

      if (event.pageX == null && event.clientX != null) {
        // @ts-ignore
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        // @ts-ignore
        event.pageX =
          event.clientX +
          ((doc && doc.scrollLeft) || (body && body.scrollLeft) || 0) -
          ((doc && doc.clientLeft) || (body && body.clientLeft) || 0);

        // @ts-ignore
        event.pageY =
          event.clientY +
          ((doc && doc.scrollTop) || (body && body.scrollTop) || 0) -
          ((doc && doc.clientTop) || (body && body.clientTop) || 0);
      }

      this.x = event.pageX;
      this.y = event.pageY;
    };
  }
}

export const mousePos = new MousePos();
