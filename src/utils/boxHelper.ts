import {Box as PBox} from 'planck-js';

export class Box extends PBox {
  width = 0;
  height = 0;
  constructor(hx, hy, ...rest) {
    super(hx / 2, hy / 2, ...rest);
    this.width = hx;
    this.height = hy;
  }
}
