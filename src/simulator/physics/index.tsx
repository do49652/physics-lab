import React from 'react';
import {bind} from 'decko';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import planck, {World, Vec2, BodyDef, Body, FixtureDef} from 'planck-js';

Object.assign(planck.internal.Settings, {velocityThreshold: 0.0001});

class Physics {
  world: World;
  canvas: HTMLCanvasElement;
  @observable paused: boolean;

  timestamp: number;
  lastTimestamp: number;

  ticker = [];

  @bind
  init(paused = false): void {
    this.world = new World({
      gravity: Vec2(0, 9.81),
    });

    this.paused = paused;
    requestAnimationFrame(this.loop);
  }

  @bind
  addBody(bodyDef?: BodyDef, fixtures?: Array<FixtureDef>): Body {
    const body = this.world.createBody(bodyDef);

    for (const fixture of fixtures || []) {
      body.createFixture(fixture);
    }

    return body;
  }

  @bind
  removeBody(body: Body): void {
    this.world.destroyBody(body);
  }

  @bind
  subscribeToTicker(func: Function): void {
    this.ticker.push(func);
  }

  @bind
  unsubscribeFromTicker(func: Function): void {
    const i = this.ticker.indexOf(func);
    if (i === -1) return;
    this.ticker.splice(i, 1);
  }

  @bind
  loop(time: number): void {
    requestAnimationFrame(this.loop);

    const deltaTime = time - (this.lastTimestamp || 0);
    this.lastTimestamp = time;

    this.ticker.forEach((func) => func());

    if (window?.paused) return;
    if (this.paused) return;

    this.timestamp = (this.timestamp || 0) + deltaTime;

    for (let body = this.world.getBodyList(); body; body = body.getNext()) {
      const userData: {beforeTick?(time: number): void} = body.getUserData();
      userData?.beforeTick?.(this.timestamp);
    }

    this.world.step(deltaTime / 1000);
  }
}

const physics = new Physics();
export default physics;

@observer
export class PhysicsProvider extends React.Component<{children(physics: boolean): JSX.Element}> {
  state = {ready: false};

  componentDidMount(): void {
    physics.init(true);
    this.setState({ready: true});
  }

  @bind
  togglePause(): void {
    physics.paused = !physics.paused;
  }

  render(): JSX.Element {
    const {children} = this.props;
    const {ready} = this.state;

    if (!ready) return null;

    return (
      <>
        <>{children(!physics.paused)}</>
      </>
    );
  }
}
