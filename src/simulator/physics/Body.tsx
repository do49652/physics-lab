import React from 'react';
import {bind} from 'decko';
import {Graphics} from 'react-pixi-fiber';
import {Vec2, BodyType, BodyDef, PolygonShape, Body as PBody, CircleShape} from 'planck-js';

import Physics from '../physics';
import materials from '../../consts/materials';

interface IBodySharedProps {
  beforeTick?(body: PBody, time: number): void;
  refBody?(body: PBody): void;
  refGraphics?(graphics: Graphics, body?: PBody, redraw?: any): void;
  onMouseMove?(body: PBody): void;
  onClick?(body: PBody, graphics: Graphics, redraw: Function): void;
  physics?: boolean;
  isSelected?(): boolean;
}

export interface IBody {
  name: string;
  collisionGroup?: number;
  type: BodyType;
  position: Vec2;
  bodyDef?: BodyDef;
  shape: PolygonShape | CircleShape;
  angle?: number;
  color?: number;
  startingVelocity?: Vec2;
  material?: {
    density: number;
    friction: number;
    restitution: number;
  };
  refs?: {body: PBody; graphics: Graphics};
}

//#region [ Body helper functions ]
const getPositionFromProps = ({position}: IBody): PIXI.Point => {
  return new PIXI.Point(position.x * 10, position.y * 10);
};

const isCircle = ({shape}: IBody): boolean => {
  return !(shape as PolygonShape).m_vertices;
};

const getVerticesFromProps = ({shape}: IBody): Array<PIXI.Point> => {
  return (shape as PolygonShape).m_vertices.map(({x, y}) => new PIXI.Point(x * 10, y * 10));
};

const getRadiusFromProps = ({shape}: IBody): number => {
  return (shape as CircleShape).m_radius * 10;
};
//#endregion

class Body extends React.Component<IBody & IBodySharedProps> {
  body: PBody = null;
  graphics: PIXI.Graphics = null;

  state = {dragging: false, moved: false, ready: false};

  @bind
  componentDidMount(): void {
    const {
      name,
      collisionGroup = 1,
      type,
      position,
      shape,
      angle = 0,
      startingVelocity = new Vec2(),
      bodyDef = {},
      material = null,
    } = this.props;

    this.body = Physics.addBody({...bodyDef, type, position}, [
      {shape, ...(material || materials.presets.WOOD)},
    ]);
    this.body.setFixedRotation(false);
    this.body.setMassData({center: new Vec2(), mass: 1, I: 1});
    this.body.setAngle(angle);
    this.body.setLinearVelocity(startingVelocity);

    this.body.getFixtureList().setFilterData({
      groupIndex: collisionGroup,
      categoryBits: collisionGroup,
      maskBits: collisionGroup,
    });

    if (this.props.refBody) {
      this.props.refBody(this.body);
    }

    this.body.setUserData({
      ...((this.body.getUserData() as object) || {}),
      name,
      beforeTick: (time: number) => {
        this.props.beforeTick?.(this.body, time);
        if (this.graphics) {
          const {x, y} = this.body.getPosition();
          const angle = this.body.getAngle();
          this.graphics.setTransform(x * 10, y * 10, 1, 1, angle);
        }
      },
    });

    window.addEventListener('mouseup', this.redraw);
    this.setState({...this.state, ready: true});
  }

  componentWillUnmount(): void {
    window.removeEventListener('mouseup', this.redraw);
    Physics.removeBody(this.body);
  }

  redraw = (): void => this.drawGraphics(this.graphics);

  @bind
  setDrag(enable: boolean): void {
    const {physics, onClick} = this.props;
    const {moved} = this.state;

    this.setState({...this.state, dragging: enable && !physics});
    window.bodyDragging = enable && !physics;

    if (!enable) {
      if (!moved) onClick?.(this.body, this.graphics, this.redraw);
      else this.setState({...this.state, moved: false});
    }
  }

  @bind
  handleMouseMove(event: PIXI.interaction.InteractionEvent): void {
    if (!this.state.dragging) return;
    if (!this.state.moved) this.setState({...this.state, moved: true});

    // @ts-ignore
    const {movementX, movementY} = event.data.originalEvent;
    const {x, y} = this.body.getPosition();
    const newPos = new Vec2(x + movementX / 10, y + movementY / 10);
    this.body.setPosition(newPos);
    const angle = this.body.getAngle();
    this.graphics.setTransform(newPos.x * 10, newPos.y * 10, 1, 1, angle);
    this.props.onMouseMove?.(this.body);
  }

  @bind
  drawGraphics(graphics: PIXI.Graphics): void {
    if (!graphics || !this.body) return;
    this.graphics = graphics;

    const {isSelected, refGraphics, color = 0xffffff} = this.props;
    refGraphics?.(this.graphics, this.body, this.redraw);

    graphics.clear();
    graphics.beginFill(color);

    if (isSelected()) graphics.lineStyle(2, 0x000000);

    if (isCircle(this.props)) {
      graphics.drawCircle(0, 0, getRadiusFromProps(this.props));
    } else {
      graphics.drawPolygon(getVerticesFromProps(this.props));
    }

    const {x, y} = this.body.getPosition();
    const angle = this.body.getAngle();
    this.graphics.setTransform(x * 10, y * 10, 1, 1, angle);

    graphics.endFill();
  }

  render(): JSX.Element {
    const {physics} = this.props;
    const {ready} = this.state;

    if (!ready) return null;

    return (
      <Graphics
        {...getPositionFromProps(this.props)}
        ref={this.drawGraphics}
        interactive
        pointerdown={this.setDrag.bind(null, true)}
        pointerup={this.setDrag.bind(null, false)}
        pointermove={!physics ? this.handleMouseMove : null}
      />
    );
  }
}

export default Body;
