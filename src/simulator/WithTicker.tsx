import React from 'react';
import physics from './physics';
import {bind} from 'decko';

export class WithTicker extends React.Component<{
  defaultValue: any;
  getValue(): any;
  condition(): boolean;
}> {
  value = null;
  afterConditionUpdateCount = 0;

  componentDidMount(): void {
    physics.subscribeToTicker(this.update);
  }

  componentWillUnmount(): void {
    physics.unsubscribeFromTicker(this.update);
  }

  @bind
  update(force = false): void {
    const {getValue, condition} = this.props;

    if (force) {
      this.value = getValue();
      this.forceUpdate();
      return;
    }

    const shouldUpdate = condition();
    if (!shouldUpdate && this.afterConditionUpdateCount >= 5) return;

    if (!shouldUpdate) this.afterConditionUpdateCount++;
    else this.afterConditionUpdateCount = 0;

    this.value = getValue();
    this.forceUpdate();
  }

  render(): JSX.Element {
    const {children, defaultValue} = this.props;
    return (
      <React.Fragment key={Math.random()}>
        {children(this.value || defaultValue, this.update.bind(null, true))}
      </React.Fragment>
    );
  }
}
