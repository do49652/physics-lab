import React from 'react';
import {bind} from 'decko';
import {Stage, Graphics} from 'react-pixi-fiber';
import {Vec2, Body as PBody, BodyType, PolygonShape, CircleShape, Circle} from 'planck-js';
import {Box} from '../utils/boxHelper';

import physics, {PhysicsProvider} from './physics';
import Body, {IBody} from './physics/Body';
import materials from '../consts/materials';

export interface IObjectProperties {
  body: PBody;
  graphics: Graphics;
}

export interface IScene {
  bodies: Array<IBody>;
  world: {
    gravity: Vec2;
  };
}

interface ISimulator {
  setActions?(actions: Record<string, Function>): void;
  doUpdate?(): void;
  setData?: any;
  scene?: IScene;
}

export class Simulator extends React.Component<ISimulator> {
  scene: IScene = {
    world: {
      gravity: new Vec2(0, 9.81),
    },
    bodies: [
      {
        name: 'Object',
        type: 'static',
        color: 0x000000,
        position: new Vec2(30, 41),
        shape: new Box(20, 2),
        material: materials.presets.WOOD,
      },
    ],
  };

  selected?: {i: number; redraw?(): void; name: string} & IObjectProperties;

  constructor(args) {
    super(args);

    const {scene} = this.props;
    if (scene) {
      this.scene = {...scene};
      this.save();
    } else {
      this.load();
    }
  }

  @bind
  save(customScene: string = null): void {
    localStorage.setItem(
      'scene',
      customScene ||
        JSON.stringify(this.scene, (key, value) => {
          if (key === 'position' || key === 'gravity') {
            return {x: value.x, y: value.y};
          } else if (key === 'shape') {
            if ((value as PolygonShape).m_vertices) {
              return {type: 'box', width: value.width, height: value.height};
            } else {
              return {type: 'circle', radius: value.getRadius()};
            }
          } else if (key === 'refs') {
            return undefined;
          }
          return value;
        }),
    );
  }

  @bind
  load(): void {
    const scene = localStorage.getItem('scene');
    if (scene) {
      try {
        this.scene = {
          world: {
            gravity: new Vec2(JSON.parse(scene).world?.gravity || {x: 0, y: 9.81}),
          },
          bodies: JSON.parse(scene).bodies.map((body) => ({
            ...body,
            position: new Vec2(body.position),
            shape:
              body.shape.type === 'circle'
                ? new Circle(body.shape.radius)
                : new Box(body.shape.width, body.shape.height),
          })),
        };
      } catch {}
    }
  }

  @bind
  emptyScene(): void {
    localStorage.removeItem('scene');
    location.reload();
  }

  componentDidMount(): void {
    const {setActions} = this.props;
    if (!setActions) return;

    physics.world.setGravity(this.scene.world.gravity);

    setActions({
      isPaused: () => physics.paused,
      togglePaused: () => {
        physics.paused = !physics.paused;
        this.props.doUpdate?.();
      },
      setPaused: (paused: boolean) => {
        physics.paused = paused;
        this.props.doUpdate?.();
      },
      addBody: (body: IBody) => {
        this.addBody(body);
      },
      duplicateBody: () => {
        this.duplicateBody();
      },
      removeBody: () => {
        this.removeBody();
      },
      isBodySelected: () => Boolean(this.selected),
      getBodies: () => this.scene.bodies,
      getSelectedBody: () => this.selected,
      isStatic: () => this.selected?.body?.getType() === 'static',
      isDynamic: () => this.selected?.body?.getType() === 'dynamic',
      setStatic: () => this.modifyBody({type: 'static'}),
      setDynamic: () => this.modifyBody({type: 'dynamic'}),
      setPosition: (position: Vec2) => this.modifyBody({position}),
      setShape: (shape: PolygonShape | CircleShape) => this.modifyBody({shape}),
      setAngle: (angle: number) => this.modifyBody({angle}),
      setGraphicsAngle: (angle: number) => this.setGraphicsAngle(angle),
      getAngle: () => this.selected?.body?.getAngle(),
      isCircle: () =>
        !Boolean((this.selected?.body?.getFixtureList().getShape() as PolygonShape).m_vertices),
      getPosition: () => this.selected?.body?.getPosition(),
      getRadius: () =>
        (this.selected?.body?.getFixtureList().getShape() as CircleShape).getRadius(),
      getSize: () => {
        const {width, height} = this.selected?.body?.getFixtureList().getShape() as Box;
        return {width, height};
      },
      save: (customScene: string) => this.save(customScene),
      load: () => this.load(),
      emptyScene: () => this.emptyScene(),
      setColor: (color: number) => this.modifyBody({color}),
      getColor: () =>
        (this.selected?.i >= 0 && this.scene.bodies[this.selected?.i].color) || 0xffffff,
      getName: () => (this.selected?.i >= 0 ? this.scene.bodies[this.selected?.i].name : 'Object'),
      setName: (name: string) => {
        if (this.selected?.i >= 0) {
          this.scene.bodies[this.selected.i].name = name;
          this.doUpdate();
        }
      },
      getCollisionGroup: () =>
        (this.selected?.i >= 0 && this.scene.bodies[this.selected?.i].collisionGroup) || 1,
      setCollisionGroup: (coll: number) => {
        if (this.selected?.i >= 0) {
          this.scene.bodies[this.selected.i].collisionGroup = coll;
          this.doUpdate();
        }
      },
      setStartingVelocity: (startingVelocity: Vec2) => this.modifyBody({startingVelocity}),
      getStartingVelocity: () =>
        (this.selected?.i >= 0 && this.scene.bodies[this.selected?.i].startingVelocity) ||
        new Vec2(),
      getMaterial: () => {
        const fixture = this.selected?.body?.getFixtureList();
        const density = fixture.getDensity();
        const friction = fixture.getFriction();
        const restitution = fixture.getRestitution();
        return {density, friction, restitution};
      },
      setMaterial: ({density, friction, restitution}) => {
        const fixture = this.selected?.body?.getFixtureList();
        const shape = fixture.getShape();
        this.selected?.body?.destroyFixture(fixture);
        this.selected?.body?.createFixture(shape, {density, friction, restitution});
        this.scene.bodies[this.selected?.i].material = {density, friction, restitution};
        this.doUpdate();
      },
      getGravity: () => physics.world.getGravity(),
      setGravity: ({x, y}) => {
        physics.world.setGravity(new Vec2(x, y));
        this.doUpdate();
      },
    });
  }

  //#region [ FUNCTIONS ]
  @bind
  addBody(body: IBody): void {
    this.scene.bodies.push(body);
    this.doUpdate();
  }

  @bind
  duplicateBody(): void {
    if (Boolean(this.selected?.body)) {
      this.scene.bodies.push({...this.scene.bodies[this.selected.i]});
      this.doUpdate();
    }
  }

  @bind
  removeBody(): void {
    this.scene.bodies.splice(this.selected.i, 1);
    this.selected = undefined;
    this.doUpdate();
  }

  @bind
  modifyBody({
    type,
    position,
    shape,
    angle,
    color,
    startingVelocity,
  }: {
    type?: BodyType;
    position?: Vec2;
    shape?: PolygonShape | CircleShape;
    angle?: number;
    color?: number;
    startingVelocity?: Vec2;
  }): void {
    if (type) {
      this.scene.bodies[this.selected.i].type = type;
      this.selected.body.setType(type);
    }

    if (position) {
      this.scene.bodies[this.selected.i].position = position;
      this.selected.body.setPosition(position);
    }

    if (shape) {
      this.scene.bodies[this.selected.i].shape = shape;
      this.selected.body.destroyFixture(this.selected.body.getFixtureList());
      this.selected.body.createFixture(shape);
    }

    if (angle >= 0 || angle < 0) {
      this.scene.bodies[this.selected.i].angle = angle;
      this.selected.body.setAngle(angle);
      this.selected.redraw?.();
    }

    if (color >= 0) {
      this.scene.bodies[this.selected.i].color = color;
    }

    if (startingVelocity) {
      this.scene.bodies[this.selected.i].startingVelocity = startingVelocity;
    }

    this.doUpdate();
  }

  @bind
  selectBody(
    physics: boolean,
    i: number,
    body: PBody,
    graphics: Graphics,
    redraw?: () => void,
  ): void {
    if (this.selected?.i === i) {
      this.selected = undefined;
    } else {
      this.selected = {i, body, graphics, redraw, name: this.scene.bodies[i].name};
    }

    if (!physics) this.doUpdate();
    else this.props.doUpdate?.();
  }

  @bind
  setGraphicsAngle(angle: number): void {
    this.scene.bodies[this.selected.i].angle = angle;
    this.selected.body.setAngle(angle);
    this.selected.redraw?.();
  }
  //#endregion

  @bind
  doUpdate(): void {
    this.forceUpdate();
    this.props.doUpdate?.();
    this.save();
  }

  render(): JSX.Element {
    const {setData} = this.props;

    return (
      <PhysicsProvider>
        {(physics): JSX.Element => (
          <Stage options={{backgroundColor: 0xffffff, width: 800, height: 600}}>
            {this.scene.bodies.map((props, i) => (
              <Body
                key={i}
                {...props}
                beforeTick={(body, time): void => {
                  const vec = body.getLinearVelocity();
                  const pos = body.getPosition();
                  setData?.(i, {
                    time,
                    velocity: vec.length(),
                    velocityVec2: `{ x: ${vec.x}, y: ${vec.y} }`,
                    positionVec2: `{ x: ${pos.x}, y: ${pos.y} }`,
                    angle: body.getAngle(),
                    angularVelocity: body.getAngularVelocity(),
                  });
                }}
                physics={physics}
                refGraphics={(graphics: Graphics, body: PBody, redraw: any): void => {
                  this.scene.bodies[i].refs = {body, graphics};
                  if (this.selected?.i === i) {
                    this.selected = {i, body, graphics, redraw, name: this.scene.bodies[i].name};
                  }
                }}
                isSelected={(): boolean => this.selected?.i === i}
                onClick={this.selectBody.bind(null, physics, i)}
                onMouseMove={(body): void => {
                  this.scene.bodies[i].position = body.getPosition();
                }}
              />
            ))}
          </Stage>
        )}
      </PhysicsProvider>
    );
  }
}

export const SimulatorMemo = React.memo(
  (props: ISimulator) => <Simulator {...props} />,
  (prevProps, nextProps) => {
    return prevProps.scene === nextProps.scene;
  },
);
