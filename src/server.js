require('light-server')({
  serve: './build',
  watchexps: [],
  noReload: true,
  port: process.env.PORT || 3000,
}).start();
