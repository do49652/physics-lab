export default {
  presets: {
    WOOD: {
      density: 0.54,
      friction: 0.42,
      restitution: 0.603,
    },
    GLASS: {
      density: 2.6,
      friction: 1.0,
      restitution: 0.658,
    },
    STEEL: {
      density: 7.2,
      friction: 0.65,
      restitution: 0.597,
    },
  },
};
