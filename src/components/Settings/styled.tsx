import styled from 'styled-components';

export const PropertiesContainer = styled.div`
  position: relative;
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 20px;
`;

export const Title = styled.div`
  font-size: 20px;
`;

export const Setting = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px 0;
  width: 340px;
`;
