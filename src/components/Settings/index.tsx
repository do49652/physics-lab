import React, {FC} from 'react';
import {Circle} from 'planck-js';
import {TabView, TabPanel} from 'primereact/tabview';
import {InputSwitch} from 'primereact/inputswitch';
import {Dropdown} from 'primereact/dropdown';
import {InputText} from 'primereact/inputtext';
import {ColorPicker} from 'primereact/colorpicker';
import {Slider} from '../Slider';
import {Box} from '../../utils/boxHelper';

import {Title, PropertiesContainer, Setting} from './styled';
import {radiansToDegrees, degreesToRadians} from '../../utils/radians';
import {WithTicker} from '../../simulator/WithTicker';
import materials from '../../consts/materials';

export const Settings: FC<{actions: any}> = ({actions}) => {
  const selected = actions?.getSelectedBody?.();

  return (
    <PropertiesContainer>
      <Title>Properties</Title>
      {selected ? (
        <TabView>
          <TabPanel header="General" key={selected?.i || -1}>
            <Setting>
              <Title>Name</Title>
              <InputText
                defaultValue={actions?.getName?.()}
                disabled={!window.teacher}
                onKeyDown={(e): void => {
                  if (e.key === 'Enter') {
                    e.currentTarget.blur();
                  }
                }}
                onBlur={(e): void => {
                  actions?.setName?.(e.currentTarget.value);
                }}
              />
            </Setting>
            <Setting>
              <Title>Collision group</Title>
              <Dropdown
                value={actions?.getCollisionGroup?.()}
                disabled={!window.teacher}
                options={[
                  {label: 'G1', value: 1},
                  {label: 'G2', value: 2},
                  {label: 'G3', value: 4},
                  {label: 'G4', value: 8},
                  {label: 'G5', value: 16},
                  {label: 'G6', value: 32},
                  {label: 'G7', value: 64},
                  {label: 'G8', value: 128},
                ]}
                onChange={(e): void => {
                  actions?.setCollisionGroup?.(parseInt(e.value));
                }}
              />
            </Setting>
          </TabPanel>
          <TabPanel header="Transform">
            <Setting>
              <Title>Is static</Title>
              <InputSwitch
                checked={actions?.isStatic?.()}
                disabled={!window.teacher}
                onChange={(): void => {
                  if (actions?.isStatic?.()) actions?.setDynamic?.();
                  else actions?.setStatic?.();
                }}
              />
            </Setting>
            <Setting>
              <Title>Type</Title>
              <Dropdown
                value={actions?.isCircle?.() ? 'circle' : 'box'}
                disabled={!window.teacher}
                options={[
                  {label: 'Circle', value: 'circle'},
                  {label: 'Box', value: 'box'},
                ]}
                onChange={(e): void => {
                  if (e.value === 'circle') {
                    actions?.setShape?.(new Circle(1));
                  } else {
                    actions?.setShape?.(new Box(1, 1));
                  }
                }}
              />
            </Setting>
            <Setting>
              <Title>Rotation</Title>
              <Slider
                key={actions?.getAngle?.() || 0}
                disabled={!window.teacher}
                defaultValue={radiansToDegrees(actions?.getAngle?.() || 0)}
                min={0}
                max={180}
                onChange={(value): void => actions?.setGraphicsAngle?.(degreesToRadians(value))}
                onSlideEnd={(value): void => actions?.setAngle?.(degreesToRadians(value))}
                style={{width: 60}}
              />
            </Setting>
            <Setting>
              <Title>Position</Title>
              <WithTicker
                key={selected?.i || -1}
                defaultValue={actions?.getPosition?.() || {x: 0, y: 0}}
                // @ts-ignore
                condition={(): boolean => window.bodyDragging}
                getValue={(): any => actions?.getPosition?.()}
              >
                {({x, y}: any, forceUpdate: any): JSX.Element => (
                  <div>
                    <div className="p-inputgroup">
                      <span className="p-inputgroup-addon">x</span>
                      <InputText
                        defaultValue={x}
                        disabled={!window.teacher}
                        type="number"
                        onKeyDown={(e): void => {
                          if (e.key === 'Enter') {
                            e.currentTarget.blur();
                          }
                        }}
                        onBlur={(e): void => {
                          actions?.setPosition?.({
                            x: parseFloat(e.currentTarget.value),
                            y: actions.getPosition().y,
                          });
                          forceUpdate();
                        }}
                      />
                    </div>
                    <div className="p-inputgroup">
                      <span className="p-inputgroup-addon">y</span>
                      <InputText
                        defaultValue={y}
                        disabled={!window.teacher}
                        onKeyDown={(e): void => {
                          if (e.key === 'Enter') {
                            e.currentTarget.blur();
                          }
                        }}
                        onBlur={(e): void => {
                          actions?.setPosition?.({
                            x: actions.getPosition().x,
                            y: parseFloat(e.currentTarget.value),
                          });
                          forceUpdate();
                        }}
                      />
                    </div>
                  </div>
                )}
              </WithTicker>
            </Setting>
            <Setting>
              <Title>Size</Title>
              {!actions?.isCircle?.() ? (
                <div>
                  <div className="p-inputgroup">
                    <span className="p-inputgroup-addon">w</span>
                    <InputText
                      defaultValue={actions.getSize().width}
                      disabled={!window.teacher}
                      type="number"
                      onKeyDown={(e): void => {
                        if (e.key === 'Enter') {
                          e.currentTarget.blur();
                        }
                      }}
                      onBlur={(e): void => {
                        actions.setShape(
                          new Box(parseFloat(e.currentTarget.value), actions.getSize().height),
                        );
                      }}
                    />
                  </div>
                  <div className="p-inputgroup">
                    <span className="p-inputgroup-addon">h</span>
                    <InputText
                      defaultValue={actions.getSize().height}
                      disabled={!window.teacher}
                      onKeyDown={(e): void => {
                        if (e.key === 'Enter') {
                          e.currentTarget.blur();
                        }
                      }}
                      onBlur={(e): void => {
                        actions.setShape(
                          new Box(actions.getSize().width, parseFloat(e.currentTarget.value)),
                        );
                      }}
                    />
                  </div>
                </div>
              ) : (
                <InputText
                  defaultValue={actions?.getRadius?.() / 2}
                  disabled={!window.teacher}
                  type="number"
                  onKeyDown={(e): void => {
                    if (e.key === 'Enter') {
                      e.currentTarget.blur();
                    }
                  }}
                  onBlur={(e): void => {
                    actions?.setShape?.(new Circle(parseFloat(e.currentTarget.value) * 2));
                  }}
                />
              )}
            </Setting>
            <Setting key={selected?.i || -1}>
              <Title>Starting velocity</Title>
              <div>
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">x</span>
                  <InputText
                    defaultValue={actions?.getStartingVelocity?.()?.x || 0}
                    disabled={!window.teacher}
                    type="number"
                    onKeyDown={(e): void => {
                      if (e.key === 'Enter') {
                        e.currentTarget.blur();
                      }
                    }}
                    onBlur={(e): void => {
                      actions?.setStartingVelocity?.({
                        x: parseFloat(e.currentTarget.value),
                        y: actions.getStartingVelocity().y,
                      });
                    }}
                  />
                </div>
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">y</span>
                  <InputText
                    defaultValue={actions?.getStartingVelocity?.()?.y || 0}
                    disabled={!window.teacher}
                    onKeyDown={(e): void => {
                      if (e.key === 'Enter') {
                        e.currentTarget.blur();
                      }
                    }}
                    onBlur={(e): void => {
                      actions?.setStartingVelocity?.({
                        x: actions.getStartingVelocity().x,
                        y: parseFloat(e.currentTarget.value),
                      });
                    }}
                  />
                </div>
              </div>
            </Setting>
          </TabPanel>
          <TabPanel header="Material">
            <Setting>
              <Title>Color</Title>
              <ColorPicker
                key={selected?.i || -1}
                disabled={!window.teacher}
                defaultColor={actions?.getColor?.()?.toString(16) || 'ffffff'}
                onChange={(e): void => actions?.setColor?.(parseInt(e.value, 16))}
              />
            </Setting>
            <Setting>
              <Title>Type</Title>
            </Setting>
            <Setting key={Math.random()}>
              <Title>Preset</Title>
              <Dropdown
                value={
                  Object.keys(materials.presets).find(
                    (preset) =>
                      JSON.stringify(materials.presets[preset]) ===
                      JSON.stringify(actions?.getMaterial?.() || {}),
                  ) || 'CUSTOM'
                }
                disabled={!window.teacher}
                options={Object.keys(materials.presets)
                  .map((material) => ({
                    label: material,
                    value: material,
                  }))
                  .concat({label: 'CUSTOM', value: 'CUSTOM'})}
                onChange={(e): void => {
                  actions?.setMaterial?.(materials.presets[e.value]);
                }}
              />
            </Setting>
            <Setting key={Math.random()}>
              <Title>Density</Title>
              <InputText
                defaultValue={actions?.getMaterial?.()?.density || 0}
                disabled={!window.teacher}
                type="number"
                onKeyDown={(e): void => {
                  if (e.key === 'Enter') {
                    e.currentTarget.blur();
                  }
                }}
                onBlur={(e): void => {
                  actions?.setMaterial?.({
                    density: parseFloat(e.currentTarget.value),
                    friction: actions.getMaterial().friction,
                    restitution: actions.getMaterial().restitution,
                  });
                }}
              />
            </Setting>
            <Setting key={Math.random()}>
              <Title>Friction</Title>
              <InputText
                defaultValue={actions?.getMaterial?.()?.friction || 0}
                disabled={!window.teacher}
                type="number"
                onKeyDown={(e): void => {
                  if (e.key === 'Enter') {
                    e.currentTarget.blur();
                  }
                }}
                onBlur={(e): void => {
                  actions?.setMaterial?.({
                    density: actions.getMaterial().density,
                    friction: parseFloat(e.currentTarget.value),
                    restitution: actions.getMaterial().restitution,
                  });
                }}
              />
            </Setting>
            <Setting key={Math.random()}>
              <Title>Restitution</Title>
              <InputText
                defaultValue={actions?.getMaterial?.()?.restitution || 0}
                disabled={!window.teacher}
                type="number"
                onKeyDown={(e): void => {
                  if (e.key === 'Enter') {
                    e.currentTarget.blur();
                  }
                }}
                onBlur={(e): void => {
                  actions?.setMaterial?.({
                    density: actions.getMaterial().density,
                    friction: actions.getMaterial().friction,
                    restitution: parseFloat(e.currentTarget.value),
                  });
                }}
              />
            </Setting>
          </TabPanel>
        </TabView>
      ) : (
        <TabView>
          <TabPanel header="Scene">
            <Setting key={selected?.i || -1}>
              <Title>Gravity</Title>
              <div>
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">x</span>
                  <InputText
                    defaultValue={actions?.getGravity?.().x}
                    disabled={!window.teacher}
                    type="number"
                    onKeyDown={(e): void => {
                      if (e.key === 'Enter') {
                        e.currentTarget.blur();
                      }
                    }}
                    onBlur={(e): void => {
                      actions?.setGravity?.({
                        x: parseFloat(e.currentTarget.value),
                        y: actions.getGravity().y,
                      });
                    }}
                  />
                </div>
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">y</span>
                  <InputText
                    defaultValue={actions?.getGravity?.().y}
                    disabled={!window.teacher}
                    onKeyDown={(e): void => {
                      if (e.key === 'Enter') {
                        e.currentTarget.blur();
                      }
                    }}
                    onBlur={(e): void => {
                      actions?.setGravity?.({
                        x: actions.getGravity().x,
                        y: parseFloat(e.currentTarget.value),
                      });
                    }}
                  />
                </div>
              </div>
            </Setting>
          </TabPanel>
        </TabView>
      )}
    </PropertiesContainer>
  );
};
