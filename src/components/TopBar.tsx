import React, {FC, useState} from 'react';
import {Menubar} from 'primereact/menubar';
import {MenuItem} from 'primereact/components/menuitem/MenuItem';
import {Vec2} from 'planck-js';
import FileSaver from 'file-saver';
import FileDialog from 'file-dialog';
import {Box} from '../utils/boxHelper';

// @ts-ignore
import freefall from '../examples/free-fall.json';

const f = (f, ...args) => (): void => f?.(...args);
const c = (...fs: any) => (): void => fs.forEach((f) => f?.());

const items = (
  {
    isPaused,
    setPaused,
    addBody,
    isBodySelected,
    duplicateBody,
    removeBody,
    clearChartData,
    save,
    emptyScene,
  },
  doUpdate,
): Array<MenuItem> => [
  {
    label: 'File',
    icon: 'fas fa-file',
    disabled: !isPaused?.(),
    items: [
      {label: 'New', disabled: !window.teacher, icon: 'fas fa-plus', command: f(emptyScene)},
      {
        label: 'Open',
        icon: 'fas fa-folder-open',
        items: [
          {
            label: 'From file',
            icon: 'fas fa-file',
            command: (): void => {
              FileDialog({multiple: false, accept: 'application/json'}).then((filelist) => {
                if (filelist.length > 0) {
                  const file = filelist.item(0);
                  file.text().then((scene) => {
                    save(scene);
                    location.reload();
                  });
                }
              });
            },
          },
          {
            label: 'Examples',
            items: [
              {
                label: 'Free fall',
                command: () => {
                  save(JSON.stringify(freefall));
                  location.reload();
                },
              },
            ],
          },
        ],
      },
      {
        label: 'Save',
        icon: 'fas fa-save',
        disabled: !window.teacher,
        command: (): void => {
          save();
          const blob = new Blob(
            [JSON.stringify(JSON.parse(localStorage.getItem('scene')), null, 2)],
            {
              type: 'application/json;charset=utf-8',
            },
          );
          FileSaver.saveAs(blob, 'scene.json');
        },
      },
    ],
  },
  {
    label: 'Objects',
    icon: 'fas fa-cubes',
    disabled: !isPaused?.() || !window.teacher,
    items: [
      {
        label: 'Add new',
        icon: 'fas fa-plus',
        command: f(addBody, {
          type: 'dynamic',
          name: 'Object',
          color: 0x000000,
          position: new Vec2(40, 20),
          shape: new Box(1, 1),
        }),
      },
      {
        label: 'Remove selected',
        icon: 'fas fa-times',
        disabled: !isBodySelected?.(),
        command: f(removeBody),
      },
      {
        label: 'Duplicate selected',
        icon: 'fas fa-clone',
        disabled: !isBodySelected?.(),
        command: f(duplicateBody),
      },
    ],
  },
  {
    label: isPaused?.() || window.paused ? 'Play' : 'Pause',
    icon: `fas ${isPaused?.() || window.paused ? 'fa-play' : 'fa-pause'}`,
    command: (): void => {
      if (isPaused?.()) {
        setPaused(false);
        window.paused = false;
      } else {
        window.paused = !window.paused;
      }
      doUpdate();
    },
  },
  {
    label: 'Stop',
    icon: 'fas fa-stop',
    disabled: isPaused?.(),
    command: c(f(setPaused, true), f(clearChartData)),
  },
  {
    label: window.teacher ? 'Logout' : 'Login as Teacher',
    command: () => {
      if (!window.teacher) {
        const pass = prompt('Enter password');
        if (pass === '123') {
          localStorage.setItem('teacher', 'true');
          location.reload();
        } else {
          alert('Worng password');
        }
      } else {
        localStorage.removeItem('teacher');
        location.reload();
      }
    },
    style: {marginLeft: 'auto'},
  },
];

export const TopBar: FC<{actions}> = ({actions}) => {
  const [tstat, doUpdate] = useState(false);

  if (!actions) return null;
  return <Menubar model={items(actions, doUpdate.bind(null, !tstat))} className="ul-full-width" />;
};
