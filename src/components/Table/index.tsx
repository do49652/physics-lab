import React from 'react';
import {bind} from 'decko';
import {throttle} from 'throttle-debounce';
import {Vec2} from 'planck-js';
import {Table as VTable, Column} from 'react-virtualized';
import physics from '../../simulator/physics';
import {exportData} from '../../utils/export';
import {Column as SColumn} from '../Layout/styled';

export interface IData {
  time: number;
  velocity: number;
  velocityVec2: Vec2;
  positionVec2: Vec2;
  angle: number;
  angularVelocity: number;
}

const rowStyle = {
  display: 'flex',
  alignItems: 'center',
  paddingRight: '10px',
  paddingLeft: '10px',
  borderBottom: 'solid 1px black',
};

export class Table extends React.Component<{setData: any; setActions: any; getSelected: any}> {
  data: IData[][] = [];

  doSetDataFunctions = [];
  lastTimestamp = 0;
  timestamp = 0;

  componentDidMount(): void {
    const {setData, setActions} = this.props;

    setData(() => (i: number, data: any): void => {
      this.doSetData(i, data);
    });

    setActions?.({
      clearChartData: () => {
        this.clearData();
      },
      updateChart: () => {
        this.forceUpdate();
      },
    });
  }

  throttledForceUpdate = throttle(100, this.forceUpdate);

  @bind
  doSetData(i: number, data: IData): void {
    if (!this.data[i]) this.data[i] = [];
    this.data[i].push({...data, time: data.time - this.timestamp});
    this.lastTimestamp = data.time;

    const index = this.props?.getSelected?.()?.i;
    if (index === i) this.throttledForceUpdate();
  }

  @bind
  clearData(): void {
    this.timestamp = this.lastTimestamp;
    this.data = [];
    this.forceUpdate();
  }

  render(): JSX.Element {
    const index = this.props?.getSelected?.()?.i;

    if (!physics.paused) {
      return (
        <SColumn>
          <VTable
            height={600}
            headerHeight={24}
            overscanRowCount={100}
            rowHeight={22}
            rowGetter={({index: i}): any => this.data?.[index]?.[i] || {}}
            rowCount={this.data?.[index]?.length || 0}
            width={800}
            rowStyle={rowStyle}
          >
            <Column
              dataKey="time"
              label="Time"
              width={150}
              cellDataGetter={({dataKey, rowData}): string =>
                `${Math.round(rowData[dataKey] * 100) / 100} ms`
              }
            />
            <Column
              dataKey="velocity"
              label="Velocity"
              width={150}
              cellDataGetter={({dataKey, rowData}): string =>
                `${Math.round(rowData[dataKey] * 100) / 100} m/s`
              }
            />
            <Column
              dataKey="velocityVec2"
              label="Velocity { x, y }"
              width={180}
              cellDataGetter={({dataKey, rowData}): string =>
                rowData[dataKey].replace(/(\d\.\d{2})\d*/g, '$1')
              }
            />
            <Column
              dataKey="positionVec2"
              label="Position { x, y }"
              width={180}
              cellDataGetter={({dataKey, rowData}): string =>
                rowData[dataKey].replace(/(\d\.\d{2})\d*/g, '$1')
              }
            />
            <Column
              dataKey="angle"
              label="Angle"
              width={150}
              style={{display: 'flex', justifyContent: 'center'}}
              headerStyle={{display: 'flex', justifyContent: 'center'}}
              cellDataGetter={({dataKey, rowData}): string =>
                `${Math.round(rowData[dataKey] * 100) / 100}`
              }
            />
            <Column
              dataKey="angularVelocity"
              label="Angular velocity"
              width={150}
              style={{display: 'flex', justifyContent: 'center'}}
              headerStyle={{display: 'flex', justifyContent: 'center'}}
              cellDataGetter={({dataKey, rowData}): string =>
                `${Math.round(rowData[dataKey] * 100) / 100}`
              }
            />
          </VTable>

          {index !== null && index !== undefined && (
            <button
              style={{width: '60%', marginLeft: '20%', height: '30px', marginTop: '20px'}}
              onClick={() => exportData(this.props?.getSelected?.()?.name, this.data?.[index])}
            >
              Export
            </button>
          )}
        </SColumn>
      );
    }

    // @ts-ignore
    return this.props?.children || null;
  }
}
