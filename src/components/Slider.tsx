import React, {FC, useState} from 'react';
import {InputText} from 'primereact/inputtext';
import {Slider as PSlider} from 'primereact/slider';

interface ISlider {
  min: number;
  max: number;
  defaultValue: number;
  onChange?(value: number): void;
  onSlideEnd(value: number): void;
  style: any;
  disabled: boolean;
}

export const Slider: FC<ISlider> = (props) => {
  const [value, setValue] = useState(props.defaultValue);

  const {onChange, onSlideEnd, disabled = false} = props;
  return (
    <>
      <InputText
        value={value}
        disabled={disabled}
        style={props.style}
        type="number"
        onChange={(e): void => {
          setValue(e.target?.value as number);
          onChange(e.target?.value as number);
        }}
      />
      <PSlider
        {...props}
        value={value}
        onChange={(e): void => {
          onChange?.(e.value as number);
          setValue(e.value as number);
        }}
        onSlideEnd={(e): void => onSlideEnd(e.value as number)}
      />
    </>
  );
};
