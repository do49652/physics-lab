import styled from 'styled-components';

export const Container = styled.div`
  position: relative;
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: stretch;
  margin: 10px 0;
`;

export const Column = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`;

export const ChartContainer = styled.div`
  display: flex;
  width: 900px;
  height: 700px;
`;
