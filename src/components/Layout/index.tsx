import React, {FC, useState} from 'react';

import {Container, Column, ChartContainer} from './styled';
import {TopBar} from '../TopBar';
import {Simulator} from '../../simulator';
import {Settings} from '../Settings';
import {Table} from '../Table';

export const Layout: FC = () => {
  const [actions, setActions] = useState(null);
  const [tstate, doUpdate] = useState(false);
  const [doSetData, setData] = useState<any>(null);

  return (
    <>
      <TopBar actions={actions} />

      <Container>
        <Column>
          {doSetData && (
            <Simulator
              doUpdate={(): void => {
                actions?.updateChart();
                if (actions?.isPaused?.()) doUpdate(!tstate);
              }}
              setActions={(newactions): void => setActions({...actions, ...newactions})}
              setData={doSetData}
            />
          )}
        </Column>

        <Column>
          <ChartContainer>
            <Table
              getSelected={actions?.getSelectedBody}
              setData={setData}
              setActions={(newactions): void => setActions({...actions, ...newactions})}
            >
              <Settings actions={actions} />
            </Table>
          </ChartContainer>
        </Column>
      </Container>
    </>
  );
};
